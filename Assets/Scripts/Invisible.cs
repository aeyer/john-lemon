﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Invisible : MonoBehaviour
{
    public GameObject player;
    public Observer observer;

    public int timeLeft = 21;
    public Text countdown;

    public MeshRenderer meshRenderer1;
    public CapsuleCollider capsuleCollider1;
    public Material meterial1;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            StartCoroutine(Example());
        }

    }

    IEnumerator Example()
    {
        observer.Invisible();
        meshRenderer1.enabled = false;
        capsuleCollider1.enabled = false;
        meterial1.color = Color.white;

        while (timeLeft != -1)
        {
            countdown.text = ("INVISIBLE:" + timeLeft);
            yield return new WaitForSeconds(1);
            timeLeft--;
        }

        Destroy(countdown);
        observer.UnInvisible();
        meterial1.color = Color.red;

    }
}