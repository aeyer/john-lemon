﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Animator m_Animator;
    private Rigidbody m_Rigibody;
    private Vector3 m_Movement;
    public Material m1;

    private bool m_IsPlayerInvisible = false;
    
    public bool M_IsPlayerInvisible
    {
        get { return m_IsPlayerInvisible; }
        set { m_IsPlayerInvisible = value; }
    }
    
    
    Quaternion m_Rotation = Quaternion.identity;
                //Quaternions are a way of storing rotations
    public float turnSpeed = 20f;
    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigibody = GetComponent<Rigidbody>();
        m1.color = Color.red;

    }

    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);  
        m_Movement.Normalize();
        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;

        m_Animator.SetBool("IsWalking", isWalking);

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement,
            turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation (desiredForward);
        
    } 
    
    void OnAnimatorMove()
    {
        m_Rigibody.MovePosition(m_Rigibody.position
        + m_Movement * m_Animator.deltaPosition.
            magnitude); 
        m_Rigibody.MoveRotation(m_Rotation);

    }
}
