﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveUN : MonoBehaviour
{
    public GameObject Cube;

    Animator animator;


    private void Start()
    {
        animator = Cube.GetComponent<Animator>();
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            Cube.SetActive(false);
            animator.enabled = false;
        }
        else
        {
            Cube.SetActive(true);
            animator.enabled = true;
        }
        
    }
}
